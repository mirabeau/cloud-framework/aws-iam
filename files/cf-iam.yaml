---
AWSTemplateFormatVersion: '2010-09-09'
Description: CloudFormation template for IAM resources
Parameters:
  CFNInitBucketName:
    Description: "S3 Bucket containing /cfn-init/ scripts"
    Type       : String
  CFNInitBucketKeyId:
    Description: "S3 Bucket KMS encryption key, empty for default"
    Type       : String

Conditions:
  HasCFNInitBucketKey: !Not [ !Equals [ !Ref CFNInitBucketKeyId, '']]

Resources:
  EC2GetIamSshUsersPolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties:
      ManagedPolicyName: "EC2GetIamSshUsersPolicy"
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Action:
              - iam:GetGroup
              - iam:GetSSHPublicKey
              - iam:ListSSHPublicKeys
            Resource: "*"

  EC2GetSSMParameterPolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties:
      ManagedPolicyName: "EC2GetSSMParameterPolicy"
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Action:
              - ssm:GetParameter
              - ssm:GetParameters
            Resource: !Sub "arn:aws:ssm:${AWS::Region}:${AWS::AccountId}:parameter/ec2/config/*"

  EC2SSHMFAParameterPolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties:
      ManagedPolicyName: "EC2SSHMFAParameterPolicy"
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Action:
              - ssm:GetParameter
              - ssm:GetParameters
              - ssm:PutParameter
            Resource: !Sub "arn:aws:ssm:${AWS::Region}:${AWS::AccountId}:parameter/ec2/config/mfa/*"

  S3GetCFNInitFilesPolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties:
      ManagedPolicyName: "S3GetCFNInitFilesPolicy"
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Action:
              - s3:ListBucket
            Resource: !Sub "arn:aws:s3:::${CFNInitBucketName}"
          - Effect: Allow
            Action:
              - s3:GetObject
            Resource: !Sub "arn:aws:s3:::${CFNInitBucketName}/cfn-init/*"
          - Effect: Allow
            Action:
              - kms:Decrypt
            Resource: !If [ HasCFNInitBucketKey, !Ref CFNInitBucketKeyId, "arn:aws:kms:${AWS::Region}:${AWS::AccountId}:key/*" ]

  SudoersUserGroup:
    Type: "AWS::IAM::Group"
    Properties:
      GroupName: sudoers

  SSHUserGroup:
    Type: "AWS::IAM::Group"
    Properties:
      GroupName: ssh-users

  ASRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: "ASRole"
      ManagedPolicyArns:
        - !Ref EC2GetIamSshUsersPolicy
        - !Ref S3GetCFNInitFilesPolicy
        - arn:aws:iam::aws:policy/AmazonEC2ReadOnlyAccess
        - arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action:
              - sts:AssumeRole

  ASProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      InstanceProfileName: "ASProfile"
      Path: "/"
      Roles:
        - !Ref ASRole

  LogRolePolicy:
    Type: AWS::IAM::ManagedPolicy
    Properties:
      ManagedPolicyName: "LogRolePolicy"
      PolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Action:
              - logs:CreateLogGroup
              - logs:CreateLogStream
              - logs:PutLogEvents
              - logs:DescribeLogStreams
            Resource:
              - 'arn:aws:logs:*:*:*'

  ECSEC2Role:
    Type: AWS::IAM::Role
    Properties:
      RoleName: "ECSEC2Role"
      ManagedPolicyArns:
        - !Ref EC2GetIamSshUsersPolicy
        - !Ref S3GetCFNInitFilesPolicy
        - !Ref LogRolePolicy
        - !Ref EC2GetSSMParameterPolicy
        - arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role
        - arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action:
              - sts:AssumeRole
      Path: "/"

  ECSEC2Profile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      InstanceProfileName: "ECSEC2Profile"
      Path: "/"
      Roles:
        - !Ref ECSEC2Role

  EKSEC2Role:
    Type: AWS::IAM::Role
    Properties:
      RoleName: "EKSEC2Role"
      ManagedPolicyArns:
        - !Ref EC2GetIamSshUsersPolicy
        - !Ref S3GetCFNInitFilesPolicy
        - !Ref LogRolePolicy
        - !Ref EC2GetSSMParameterPolicy
        - arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy
        - arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy
        - arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action:
              - sts:AssumeRole
      Path: "/"

  EKSEC2Profile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      InstanceProfileName: "EKSEC2Profile"
      Path: "/"
      Roles:
        - !Ref EKSEC2Role

  ECSServiceRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: "ECSServiceRole"
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceRole
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ecs.amazonaws.com
            Action:
              - sts:AssumeRole
      Path: "/"

  ECSRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: "ECSRole"
      ManagedPolicyArns:
        - !Ref EC2GetIamSshUsersPolicy
        - !Ref S3GetCFNInitFilesPolicy
        - arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceRole
        - arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceforEC2Role
        - arn:aws:iam::aws:policy/AmazonS3ReadOnlyAccess
      AssumeRolePolicyDocument:
        Version: '2012-10-17'
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action:
              - sts:AssumeRole
      Path: "/"

Outputs:
  EC2GetIamSshUsersPolicyARN:
    Value: !Ref EC2GetIamSshUsersPolicy
    Export:
      Name: EC2GetIamSshUsersPolicyARN
  EC2GetSSMParameterPolicyARN:
    Value: !Ref EC2GetSSMParameterPolicy
    Export:
      Name: EC2GetSSMParameterPolicyARN
  EC2SSHMFAParameterPolicyARN:
    Value: !Ref EC2SSHMFAParameterPolicy
    Export:
      Name: EC2SSHMFAParameterPolicyARN
  S3GetCFNInitFilesPolicyARN:
    Value: !Ref S3GetCFNInitFilesPolicy
    Export:
      Name: S3GetCFNInitFilesPolicyARN
  ASProfile:
    Value: !Ref ASProfile
    Export:
      Name: ASProfile
  ASProfileARN:
    Value: !GetAtt ASProfile.Arn
    Export:
      Name: ASProfileARN
  ECSRole:
    Value: !Ref ECSRole
    Export:
      Name: ECSRole
  ECSEC2Role:
    Value: !Ref ECSEC2Role
    Export:
      Name: ECSEC2Role
  ECSEC2Profile:
    Value: !Ref ECSEC2Profile
    Export:
      Name: ECSEC2Profile
  ECSEC2ProfileARN:
    Value: !GetAtt ECSEC2Profile.Arn
    Export:
      Name: ECSEC2ProfileARN
  EKSEC2Role:
    Value: !Ref EKSEC2Role
    Export:
      Name: EKSEC2Role
  EKSEC2RoleARN:
    Value: !GetAtt EKSEC2Role.Arn
    Export:
      Name: EKSEC2RoleARN
  EKSEC2Profile:
    Value: !Ref EKSEC2Profile
    Export:
      Name: EKSEC2Profile
  EKSEC2ProfileARN:
    Value: !GetAtt EKSEC2Profile.Arn
    Export:
      Name: EKSEC2ProfileARN
  ECSServiceRole:
    Value: !Ref ECSServiceRole
    Export:
      Name: ECSServiceRole
  LogRolePolicyARN:
    Value: !Ref LogRolePolicy
    Export:
      Name: LogRolePolicyARN
  SudoersUserGroup:
    Value: !Ref SudoersUserGroup
    Export:
      Name: SudoersUserGroup
  SSHUserGroup:
    Value: !Ref SSHUserGroup
    Export:
      Name: SSHUserGroup
