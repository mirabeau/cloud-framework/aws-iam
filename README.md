AWS IAM
=======
This role is part of the [Mirabeau Cloud Framework](https://gitlab.com/mirabeau/cloud-framework/)

Create Cloudformation stack for IAM roles and policies. AWS resources that will be created are:
 * EC2GetIamSshUsersPolicy
 * EC2GetSSMParameterPolicy
 * EC2SSHMFAParameterPolicy
 * S3GetCFNInitFilesPolicy
 * LogRolePolicy
 * ECSServiceRole
 * ECSRole
 * ECSEC2Role
 * EKSEC2Role
 * ECSEC2Profile
 * EKSEC2Profile
 * ASRole
 * ASProfile
 * ssh-users Group
 * sudoers Group

![Draw.io](draw.io/services.png)

A test playbook has been supplied which rolls out the IAM resources included with this role.
You can run this playbook using the following command:
```bash
ansible-playbook aws-iam/tests/test.yml --inventory aws-iam/tests/inventory.yml
```
This command should run outside of the role dir and requires the aws-utils and aws-lambda role to be in the same root dir as well.

Requirements
------------
Ansible version 2.5.4 or higher  
Python 2.7.x  

Required python modules:
* boto
* boto3
* awscli

Dependencies
------------
None

Role Variables
--------------
### _Internal_
```yaml
lambda_stack_prefix: "lambda"
```
### _General_
The following params should be available for Ansible during the rollout of this role:
```yaml
aws_region      : <aws region, eg: eu-west-1>
owner           : <owner, eg: mirabeau>
account_name    : <aws account name>
account_abbr    : <aws account generic environment>
```

Role Defaults
--------------
```yaml
create_changeset     : True
debug                : False
cloudformation_tags  : {}
tag_prefix           : "mcf"
private_bucket_name: "{{ account_name }}-private"

aws_iam_params:
  create_changeset   : "{{ create_changeset }}"
  debug              : "{{ debug }}"

  s3_cfninit_kms_key : ""
  cfninit_bucket_name: "{{ private_bucket_name }}"
```

Example Playbook
----------------
```yaml
---
- hosts: localhost
  connection: local
  gather_facts: False
  vars:
    aws_region      : "eu-west-1"
    owner           : "a-company"
    account_name    : "a-com-dta"

  roles:
    - aws-iam
```

License
-------
GPLv3

Author Information
------------------
Lotte-Sara Laan <llaan@mirabeau.nl>
